﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcessionTest.Métier
{
    public class Client
    {
        public string nom { get; set; }
        public string prenom { get; set; }
        public Vehicule vehiculeAchete { get; set; }

        public Client(){

        }
    }
}
