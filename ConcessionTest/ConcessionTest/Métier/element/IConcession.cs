﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcessionTest.Métier.element
{
    interface IConcession
    {
        void VendreVehicule(Client client, Vehicule vehicule);
        void AcheterVehicule();
        void MiseEnVente();
        void MiseEnAttente();
        Vehicule MeilleureVente();
    }
}
