﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcessionTest.Métier.element
{
    public static class ElementAide
    {
        public const int AFFICHER_CLIENTS = 3;
        public const int AFFICHER_CONCESSION = 1;
        public const int AFFICHER_VEHICULES = 2;
        public const int ACHAT_VEHICULE = 4;
        public const int VENTE_VEHICULE = 5;

    }
}
