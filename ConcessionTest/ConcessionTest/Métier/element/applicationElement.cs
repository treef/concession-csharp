﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcessionTest.Métier.element
{
    public static class applicationElement
    {

        public static void MenuBaseDisplay(){
            Console.WriteLine("1 - Afficher les infos de la concession");
            Console.WriteLine("2 - Afficher les infos des voitures");
            Console.WriteLine("3 - Afficher les infos des clients");
            Console.WriteLine("4 - Achat d'une voiture");
            Console.WriteLine("5 - Vente d'une voiture");
            Console.WriteLine();
        }

        public static void MenuListVoiture(List<Vehicule> vehicules)
        {
            int i = 0;
            foreach(Vehicule vehicule in vehicules)
            {
                i++;
                Console.WriteLine(Convert.ToString(i) + " - " + vehicule.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
        } 

        public static int ChoixMenu()
        {
            Console.WriteLine("Entrer un chiffre en fonction du menu (entre 1 et 5 compris)");
            string nombre = Console.ReadLine();
            int number = 0;
            while(nombre == null)
            {
                nombre = Console.ReadLine();
            }
            number = Convert.ToInt32(nombre);
            
            
            string reveal= string.Empty;
            while (reveal == string.Empty)
            {
                if(number<=5 && number>=1)
                {
                    reveal = "Tu sors";
                }
                else
                {
                    Console.WriteLine("Entrer un chiffre en fonction du menu (entre 1 et 5 compris)");
                    nombre = Console.ReadLine();
                    number = Convert.ToInt32(nombre);
                }
            }

            return number;
        }
    }
}
