﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcessionTest.Métier
{
    public static class DummyData
    {

        #region Partie Vehicule
        /// <summary>
        /// Création véhicule par entré clavier
        /// </summary>
        /// <returns>Vehicule</returns>
        public static Vehicule CreateVehicule()
        {
            Vehicule vehicule = new Vehicule();
            Console.WriteLine("Quelle est la marque du vehicule");
            vehicule.Marque = Console.ReadLine();
            Console.WriteLine("Quelle est la modéle du vehicule");
            vehicule.Modele = Console.ReadLine();
            Console.WriteLine("Quelle est la Annee du vehicule");
            vehicule.Annee = Console.ReadLine();
            Console.WriteLine("Quelle est la raison social du vehicule");
            vehicule.raisonStatus = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Quelle est le Status du vehicule");
            vehicule.status = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Quelle est la type vehicule du vehicule");
            vehicule.TypeVehicule = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Quelle est la typeVente du vehicule");
            vehicule.TypeVente = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Quelle est la valeur du vehicule");
            vehicule.Valeur = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Quelle est la nombre de roue du vehicule");
            vehicule.roue = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Quelle est le chassis du vehicule");
            vehicule.chassis = Console.ReadLine();
            Console.WriteLine("Quelle est la circulation du vehicule");
            vehicule.circulation = Convert.ToInt32(Console.ReadLine());
            return vehicule;
        }

        /// <summary>
        /// Create random vehicule
        /// </summary>
        /// <returns>Vehicule</returns>
        public static Vehicule CreateDummyVehicule()
        {
            Vehicule vehicule = new Vehicule();
            Random random = new Random();
            int[] roues = { 1, 2, 4, 8};
            vehicule.Marque = RandomString(10);
            vehicule.Modele = RandomString(8);
            vehicule.Annee = RandomString(4);
            vehicule.raisonStatus = random.Next(1, 5);
            
            vehicule.status = random.Next(1, 5);
            
            vehicule.TypeVehicule = random.Next(1, 5);
            //vehicule.TypeVente = random.Next(1, 5);
            
            vehicule.Valeur = random.Next(350, 100000);
            vehicule.roue = roues[random.Next(roues.Length)];
            vehicule.chassis = RandomString(7) + " " + RandomString(5);
            vehicule.circulation = random.Next(1, 5);

            return vehicule;
        }

        public static List<Vehicule> DummyListVehicule()
        {
            List<Vehicule> vehicules = new List<Vehicule>();

            for (int i = 0; i < 10; i++)
            {
                vehicules.Add(CreateDummyVehicule());
            }

            return vehicules;
        }
        #endregion

        #region Partie Concession
        /// <summary>
        /// Création concession par entré clavier
        /// </summary>
        /// <returns>Concession</returns>
        public static Concession CreateConcession()
        {
            Concession concession = new Concession();
            return concession;
        }

        /// <summary>
        /// Create random Concession
        /// </summary>
        /// <returns>Concession</returns>
        public static Concession CreateDummyConcession(List<Vehicule> vehicules)
        {
            Concession concession = new Concession();
            Random random = new Random();
            concession.CA = random.Next(100000, 500000000);
            concession.Effectif = random.Next(4, 150);
            concession.RaisonSocial = "Dummy Concession";
            concession.ConcessionVehicule = vehicules;
            return concession;
        }
        #endregion

        #region Partie Client
        /// <summary>
        /// Création client par entré clavier
        /// </summary>
        /// <returns>Client</returns>
        public static Client CreateClient()
        {
            Client client = new Client();
            return client;
        }

        /// <summary>
        /// Create random client
        /// </summary>
        /// <returns>Client</returns>
        public static Client CreateDummyClient()
        {
            Client client = new Client();
            return client;
        }
        #endregion

        public static string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
