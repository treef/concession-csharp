﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcessionTest.Métier
{
    /// <summary>
    /// Description d'un vehicule
    /// </summary>
    public class Vehicule
    {
        /// <summary>
        /// moto, voiture, camion, ...
        /// </summary>
        public int TypeVehicule { get; set; }
        /// <summary>
        /// valeur voiture
        /// </summary>
        public int Valeur { get; set; }
        /// <summary>
        /// marque de la voiture
        /// </summary>
        public string Marque { get; set; }
        /// <summary>
        /// location, vente neuf, vente occaz.
        /// </summary>
        public int TypeVente { get; set; }
        /// <summary>
        /// Annee de mise en ciculation
        /// </summary>
        public string Annee { get; set; }
        /// <summary>
        /// modele du vehicule
        /// </summary>
        public string Modele { get; set; }
        /// <summary>
        /// nom chassis
        /// </summary>
        public string chassis { get; set; }
        /// <summary>
        /// nb roue
        /// </summary>
        public int roue { get; set; }
        /// <summary>
        /// en vente, pas en vente, indispo.
        /// </summary>
        public int status { get; set; }
        /// <summary>
        /// en lavage, en réparation, en presentation, pas a la concession. 
        /// </summary>
        public int raisonStatus { get; set; }
        /// <summary>
        /// citadine, berline, limou, 4X4
        /// </summary>
        public int circulation { get; set; }

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Vehicule(){

        }

        /// <summary>
        /// Constructeur d'un véhicule en entier
        /// </summary>
        /// <param name="typeVehicule"></param>
        /// <param name="valeur"></param>
        /// <param name="marque"></param>
        /// <param name="typeVente"></param>
        /// <param name="annee"></param>
        /// <param name="modele"></param>
        public Vehicule(int typeVehicule, int valeur, string marque, int typeVente, string annee, string modele)
        {
            this.Annee = annee;
            this.Marque = marque;
            this.Modele = modele;
            this.TypeVehicule = typeVehicule;
            this.Valeur = valeur;
            this.TypeVente = typeVente;
        }

        public override string ToString()
        {
            return "La voiture de marque " + this.Marque + " et de modéle " + this.Modele + " a été mise en circulation en " + this.Annee + " a le status " + this.status + " dans la concession parce que " + this.raisonStatus+ ". Ce véhicule est type " + this.roue + " est de type de circulation " + this.circulation + ". Elle est de type " + this.TypeVehicule + " avec la valeur " + this.Valeur + "€";
        }
    }
}
