﻿using ConcessionTest.Métier.element;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcessionTest.Métier
{
    public class Concession
    {
        /// <summary>
        /// Raison social de la concession, nom de l'entreprise/concession
        /// </summary>
        public string RaisonSocial { get; set; }
        /// <summary>
        /// effectif de la concession
        /// </summary>
        public int Effectif { get; set; }
        /// <summary>
        /// Chiffre d'affaire de l'année derniere
        /// </summary>
        public float CA { get; set; }
        /// <summary>
        /// vehicule dans la concession
        /// </summary>
        public List<Vehicule> ConcessionVehicule { get; set; }
        public List<Vehicule> VehiculeVendu { get; set; }
        /// <summary>
        /// Liste des clients  
        /// </summary>
        public List<Client> Clients { get; set; }
        public float CompteAchat { get; set; }

        /// <summary>
        /// constructeur 
        /// </summary>
        /// <param name="raisonSoc"></param>
        /// <param name="eff"></param>
        /// <param name="ca"></param>
        public Concession(string raisonSoc, int eff, float ca)
        {
            this.RaisonSocial = raisonSoc;
            this.Effectif = eff;
            this.CA = ca;
            this.ConcessionVehicule = new List<Vehicule>();
            this.VehiculeVendu = new List<Vehicule>();
            this.Clients = new List<Client>();
        }

        /// <summary>
        /// constructeur
        /// </summary>
        /// <param name="raisonSoc"></param>
        /// <param name="eff"></param>
        public Concession(string raisonSoc, int eff)
        {
            this.RaisonSocial = raisonSoc;
            this.Effectif = eff;
            this.CA = 0;
            this.ConcessionVehicule = new List<Vehicule>();
            this.VehiculeVendu = new List<Vehicule>();
            this.Clients = new List<Client>();
        }

        /// <summary>
        /// constructeur
        /// </summary>
        /// <param name="raisonSoc"></param>
        public Concession(string raisonSoc)
        {
            this.RaisonSocial = raisonSoc;
            this.Effectif = 3;
            this.CA = 100000;
            this.ConcessionVehicule = new List<Vehicule>();
            this.VehiculeVendu = new List<Vehicule>();
            this.Clients = new List<Client>();
        }

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Concession()
        {
            this.ConcessionVehicule = new List<Vehicule>();
            this.VehiculeVendu = new List<Vehicule>();
            this.Clients = new List<Client>();
        }

        /// <summary>
        /// Override tostring de la classe Concession
        /// </summary>
        /// <returns></returns>
        public override string ToString(){
            return "La concession " + this.RaisonSocial + " a un effectif de " + this.Effectif.ToString() + " et un CA de " + this.CA + " dispose de " + this.ConcessionVehicule.Count + " Vehicule(s).";
        }

        /// <summary>
        /// Vente d'un vehicule
        /// </summary>
        /// <param name="client"></param>
        /// <param name="vehicule"></param>
        public void VendreVehicule(Client client)
        {
            Vehicule vehicule = new Vehicule();
            this.DisplayListVehicule();
            Console.WriteLine("Veuillez valider par o (oui) ou n (non) la vente");
            Console.WriteLine("Tapez le numéro de la voiture qui est vendu ");
            int index = Convert.ToInt32(Console.ReadLine());
            if(Console.ReadLine() == "o"){
                vehicule = this.ConcessionVehicule.ElementAt(index);
                client.vehiculeAchete = vehicule;
                this.Clients.Add(client);
                this.ConcessionVehicule.Remove(vehicule);
                this.CompteAchat = this.CompteAchat + vehicule.Valeur;
            }
            else
            {
                Console.WriteLine("Le vehicuele n'a pas été vendu");
            }
        }

        /// <summary>
        /// Achat d'un vehicule
        /// </summary>
        public void AcheterVehicule()
        {
            Vehicule vehicule = DummyData.CreateVehicule();
            this.ConcessionVehicule.Add(vehicule);
            this.Clients.Add(DummyData.CreateClient());
            this.CompteAchat = this.CompteAchat - vehicule.Valeur;
        }

        /// <summary>
        /// VehiculeMise en vente
        /// </summary>
        public void MiseEnVente()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Vehicule MeilleureVente()
        {
            Vehicule vehicule = new Vehicule();
            return vehicule;
        }

        public void MiseEnAttente()
        {

        }

        public void DisplayConcessionToString()
        {
            Console.WriteLine(this.ToString());
        }

        public void DisplayListVehicule()
        {
            if (this.Clients.Count > 0)
            {
                foreach (Vehicule vehicule in this.ConcessionVehicule)
                {
                    Console.WriteLine(vehicule.ToString());
                }
            }
            else
            {
                Console.WriteLine("Aucune voiture à vendre");
            }
        }

        public void DisplayListClient()
        {
            if(this.Clients.Count > 0)
            {
                foreach (Client client in this.Clients)
                {
                    Console.WriteLine(client.ToString());
                }
            }
            else
            {
                Console.WriteLine("Vous avez aucun Client");
            }
            
        }
    }
}
