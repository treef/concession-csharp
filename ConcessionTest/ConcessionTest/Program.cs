﻿using ConcessionTest.Métier;
using ConcessionTest.Métier.element;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Security.Principal;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;

namespace ConcessionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Vehicule> listVehicules = new List<Vehicule>();
            listVehicules = DummyData.DummyListVehicule();
            Concession concession = new Concession();
            concession = DummyData.CreateDummyConcession(listVehicules);
            
            string ConnexionString = ConfigurationManager.AppSettings["SQLConnexionStringLocal"].Replace("NomMachine", Environment.MachineName) + WindowsIdentity.GetCurrent().Name.Split(new string[] { @"\" }, StringSplitOptions.None)[1];
            SqlConnection connexion = new SqlConnection(ConnexionString);
            applicationElement.MenuBaseDisplay();
            
            string recup = "select * from concession";

            List<string> list = typeof(Vehicule)
            .GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
            .Select(field => field.Name)
            .ToList();

            if (connexion.State == ConnectionState.Open)
            {
                connexion.Close();
            } 
            connexion.Open();

            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(recup, connexion);
            adapter.Fill(table);
            connexion.Close();

            var array = table.Rows;
            //var colonnes = table.Columns[0].DataType.Name;
            string conc = array[0][0].ToString() + " - " + array[0][1].ToString() + " avec un capital de  " + array[0][2].ToString();
            var ok = array[0]["raisonsocial"];
            string conc2 = array[1][0].ToString() + " - " + array[1][1].ToString() + " avec un capital de  " + array[1][2].ToString();
            Console.WriteLine(array[0][0]);
            

            string continuer = "o";
            while(continuer == "o")
            {
                int choix = applicationElement.ChoixMenu();
                switch (choix)
                {
                    case ElementAide.AFFICHER_CONCESSION:
                        concession.DisplayConcessionToString();
                        break;
                    case ElementAide.AFFICHER_VEHICULES:
                        concession.DisplayListVehicule();
                        break;
                    case ElementAide.AFFICHER_CLIENTS:
                        concession.DisplayListClient();
                        break;
                    case ElementAide.ACHAT_VEHICULE:
                        concession.AcheterVehicule();
                        break;
                    case ElementAide.VENTE_VEHICULE:
                        concession.VendreVehicule(DummyData.CreateDummyClient());
                        break;
                    default:
                        Console.WriteLine("What ?");
                        break;
                }

                Console.WriteLine("Voulez vous continuer (o=> oui, n=>non");
                continuer = Console.ReadLine();
                while(continuer != "o" && continuer != "n")
                {
                    Console.WriteLine("T'es lourd lis un peu, (o=> oui, n=>non");
                    continuer = Console.ReadLine();
                }
            }
            
            //applicationElement.MenuListVoiture(listVehicules);
            Console.WriteLine("Done,");
            Console.WriteLine("Appuyer sur une touche pour l'arreter");
            Console.ReadKey();
        }
    }
}
